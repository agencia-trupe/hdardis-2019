<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuemSomosTable extends Migration
{
    public function up()
    {
        Schema::create('quem_somos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->text('missao');
            $table->text('visao');
            $table->text('valores');
            $table->timestamps();
        });

        Schema::create('quem_somos_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('quem_somos_imagens');
        Schema::drop('quem_somos');
    }
}
