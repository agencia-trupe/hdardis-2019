<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipamentosParaLocacaoTable extends Migration
{
    public function up()
    {
        Schema::create('equipamentos_para_locacao', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('equipamentos_para_locacao');
    }
}
