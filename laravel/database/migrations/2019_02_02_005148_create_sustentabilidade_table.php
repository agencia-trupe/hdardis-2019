<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSustentabilidadeTable extends Migration
{
    public function up()
    {
        Schema::create('sustentabilidade', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto_home');
            $table->string('imagem_1');
            $table->text('texto_1');
            $table->text('texto_2');
            $table->string('imagem_2');
            $table->string('imagem_3');
            $table->text('texto_3');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('sustentabilidade');
    }
}
