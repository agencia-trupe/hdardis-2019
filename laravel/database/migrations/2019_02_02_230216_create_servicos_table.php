<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicosTable extends Migration
{
    public function up()
    {
        Schema::create('servicos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo');
            $table->text('texto');
            $table->string('imagem_home');
            $table->timestamps();
        });

        Schema::create('servicos_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('servico_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('servico_id')->references('id')->on('servicos')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('servicos_imagens');
        Schema::drop('servicos');
    }
}
