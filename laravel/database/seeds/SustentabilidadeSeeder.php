<?php

use Illuminate\Database\Seeder;

class SustentabilidadeSeeder extends Seeder
{
    public function run()
    {
        DB::table('sustentabilidade')->insert([
            'texto_home' => '',
            'imagem_1' => '',
            'texto_1' => '',
            'texto_2' => '',
            'imagem_2' => '',
            'imagem_3' => '',
            'texto_3' => '',
        ]);
    }
}
