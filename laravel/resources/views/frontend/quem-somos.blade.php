@extends('frontend.common.template')

@section('content')

    <div class="main quem-somos">
        <div class="center texto">
            <p>{!! $quemSomos->texto !!}</p>
        </div>

        <div class="missao-visao-valores">
            <div class="center">
                <div class="missao">
                    <div class="icone"></div>
                    <h3>MISSÃO</h3>
                    <p>{!! $quemSomos->missao !!}</p>
                </div>
                <div class="visao">
                    <div class="icone"></div>
                    <h3>VISÃO</h3>
                    <p>{!! $quemSomos->visao !!}</p>
                </div>
                <div class="valores">
                    <div class="icone"></div>
                    <h3>VALORES</h3>
                    <p>{!! $quemSomos->valores !!}</p>
                </div>
            </div>
        </div>

        <div class="center">
            <div class="imagens">
                @foreach($imagens as $imagem)
                <img src="{{ asset('assets/img/quem-somos/imagens/'.$imagem->imagem) }}" alt="">
                @endforeach
            </div>
        </div>
    </div>

@endsection
