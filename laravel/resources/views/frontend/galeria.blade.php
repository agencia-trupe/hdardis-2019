@extends('frontend.common.template')

@section('content')

    <div class="main galeria">
        <div class="center">
            @foreach($imagens as $imagem)
            <a href="{{ asset('assets/img/galeria/'.$imagem->imagem) }}" class="fancybox" rel="galeria" title="{{ $imagem->legenda }}">
                <img src="{{ asset('assets/img/galeria/thumbs/'.$imagem->imagem) }}" alt="">
            </a>
            @endforeach
        </div>
    </div>

@endsection
