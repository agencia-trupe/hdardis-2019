@extends('frontend.common.template')

@section('content')

    <div class="main contato">
        <div class="center">
            <div class="informacoes">
                <p class="telefones">{!! $contato->telefones !!}</p>
                <p class="endereco">{!! $contato->endereco !!}</p>
            </div>

            <form action="{{ route('contato.post') }}" method="POST">
                {!! csrf_field() !!}

                <h3>FALE CONOSCO</h3>

                <div class="group">
                    <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                    <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                    <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}">
                </div>
                <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
                <button></button>

                @if($errors->any())
                <div class="flash flash-erro">
                    @foreach($errors->all() as $error)
                    {!! $error !!}<br>
                    @endforeach
                </div>
                @endif

                @if(session('enviado'))
                    <div class="flash flash-sucesso">
                        Mensagem enviada com sucesso!
                    </div>
                @endif
            </form>
        </div>

        <div class="mapa">{!! $contato->google_maps !!}</div>
    </div>

@endsection
