    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ config('app.name') }}</a>
            <nav>
                @include('frontend.common.nav')
            </nav>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>
        <div class="social">
            <div class="center">
            @foreach(['linkedin', 'facebook', 'instagram'] as $s)
                @if($contato->{$s})
                    <a href="{{ Tools::parseLink($contato->{$s}) }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
                @endif
            @endforeach
            </div>
        </div>
    </header>

    <nav id="nav-mobile">
        <div class="center">
            @include('frontend.common.nav')
        </div>
    </nav>
