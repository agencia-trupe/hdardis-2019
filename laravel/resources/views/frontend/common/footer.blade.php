    <footer>
        <div class="center">
            <div class="links">
                <a href="{{ route('quem-somos') }}">QUEM SOMOS | missão, visão e valores</a>
                <a href="{{ route('servicos') }}">
                    SERVIÇOS |
                    @foreach($servicos as $s)
                    {{ mb_strtolower($s->titulo) }}
                    @if($s != $servicos->last()) &middot; @endif
                    @endforeach
                </a>
                <a href="{{ route('equipamentos') }}">
                    EQUIPAMENTOS PARA LOCAÇÃO |
                    @foreach($equipamentos as $e)
                    {{ mb_strtolower($e->tipo) }}
                    @if($e != $equipamentos->last()) &middot; @endif
                    @endforeach
                </a>
                <a href="{{ route('sustentabilidade') }}">SUSTENTABILIDADE</a>
                <a href="{{ route('galeria') }}">GALERIA</a>
                <a href="{{ route('clientes') }}">CLIENTES</a>
                <a href="{{ route('contato') }}">CONTATO</a>
            </div>

            <div class="hdardis">
                <img src="{{ asset('assets/img/layout/marca-hdardis.png') }}" alt="">
                <p class="endereco">{!! $contato->endereco !!}</p>
            </div>

            <div class="informacoes">
                <p class="telefones">{!! $contato->telefones !!}</p>
                <div class="social">
                    @foreach(['linkedin', 'facebook', 'instagram'] as $s)
                        @if($contato->{$s})
                            <a href="{{ Tools::parseLink($contato->{$s}) }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>

        <div class="copyright">
            <p>
                © {{ date('Y') }} {{ config('app.name') }} - Todos os direitos reservados.
                <span>|</span>
                <a href="http://www.trupe.net" target="_blank">Criação de Sites</a>:
                <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </footer>
