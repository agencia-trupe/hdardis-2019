<a href="{{ route('quem-somos') }}" @if(Tools::routeIs('quem-somos')) class="active" @endif>Quem <br>Somos</a>
<a href="{{ route('servicos') }}" @if(Tools::routeIs('servicos')) class="active" @endif>Serviços</a>
<a href="{{ route('equipamentos') }}" @if(Tools::routeIs('equipamentos')) class="active" @endif>Equipamentos <br>para Locação</a>
<a href="{{ route('sustentabilidade') }}" @if(Tools::routeIs('sustentabilidade')) class="active" @endif>Sustentabilidade</a>
<a href="{{ route('galeria') }}" @if(Tools::routeIs('galeria')) class="active" @endif>Galeria</a>
<a href="{{ route('clientes') }}" @if(Tools::routeIs('clientes')) class="active" @endif>Clientes</a>
<a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>Contato</a>

