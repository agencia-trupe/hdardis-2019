@extends('frontend.common.template')

@section('content')

    <div class="main sustentabilidade">
        <div class="center">
            <img src="{{ asset('assets/img/sustentabilidade/'.$sustentabilidade->imagem_1) }}" alt="">
            <div class="texto">{!! $sustentabilidade->texto_1 !!}</div>
        </div>

        <div class="cinza">
            <div class="center">
                <img src="{{ asset('assets/img/sustentabilidade/'.$sustentabilidade->imagem_2) }}" alt="">
                <div class="texto">{!! $sustentabilidade->texto_2 !!}</div>
            </div>
        </div>

        <div class="center">
            <img src="{{ asset('assets/img/sustentabilidade/'.$sustentabilidade->imagem_3) }}" alt="">
            <div class="texto">{!! $sustentabilidade->texto_3 !!}</div>
        </div>
    </div>

@endsection
