@extends('frontend.common.template')

@section('content')

    <div class="main servicos">
        <div class="center">
            <nav>
                @foreach($servicos as $s)
                <a href="{{ route('servicos', $s->slug) }}" @if($s->slug == $servico->slug) class="active" @endif>{{ $s->titulo }}</a>
                @endforeach
            </nav>

            <div class="servico">
                {!! $servico->texto !!}

                <div class="imagens">
                    @foreach($servico->imagens as $imagem)
                    <img src="{{ asset('assets/img/servicos/imagens/'.$imagem->imagem) }}" alt="">
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
