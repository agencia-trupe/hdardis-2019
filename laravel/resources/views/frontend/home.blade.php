@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="banners">
            @foreach($banners as $banner)
            <div class="banner" style="background-image:url({{ asset('assets/img/banners/'.$banner->imagem) }})">
                <div class="center">
                    <div class="texto">
                        <span>{!! $banner->texto !!}</span>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="cycle-pager"></div>
        </div>

        <div class="servicos-home">
            <div class="center">
                @foreach($servicos as $servico)
                <a href="{{ route('servicos', $servico->slug) }}" class="servico">
                    <img src="{{ asset('assets/img/servicos/'.$servico->imagem_home) }}" alt="">
                    <span>{{ $servico->titulo }}</span>
                </a>
                @endforeach
            </div>
        </div>

        <a href="{{ route('sustentabilidade') }}" class="sustentabilidade-home">
            <div class="center">
                <p>{!! $sustentabilidade->texto_home !!}</p>
                <span>sustentabilidade</span>
            </div>
        </a>
    </div>

@endsection
