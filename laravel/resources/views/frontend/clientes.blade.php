@extends('frontend.common.template')

@section('content')

    <div class="main clientes">
        <div class="center">
            <div class="clientes-thumbs">
                @foreach($clientes as $cliente)
                <div class="cliente">
                    <img src="{{ asset('assets/img/clientes/'.$cliente->imagem) }}" alt="">
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
