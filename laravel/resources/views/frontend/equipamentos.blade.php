@extends('frontend.common.template')

@section('content')

    <div class="main equipamentos">
        <div class="center">
            <nav>
                @foreach($equipamentos as $e)
                <a href="{{ route('equipamentos', $e->slug) }}">
                    <img src="{{ asset('assets/img/equipamentos/'.$e->capa) }}" alt="">
                    @if($equipamento->tipo != 'Equipamentos para Locação' && $e->slug != $equipamento->slug)
                        <div class="overlay"></div>
                    @endif
                    <span @if($e->slug == $equipamento->slug || ($equipamento->tipo != 'Equipamentos para Locação' && $e->slug != $equipamento->slug)) class="active" @endif>{{ $e->tipo }}</span>
                </a>
                @endforeach
            </nav>

            <div class="equipamento @if($equipamento->tipo != 'Equipamentos para Locação') selecionado @endif">
                <h2>{{ $equipamento->tipo }}</h2>
                {!! $equipamento->texto !!}
            </div>
        </div>
    </div>

@endsection
