@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            Quem Somos
            <a href="{{ route('painel.quem-somos.imagens.index') }}" class="btn btn-info btn-sm pull-right"><span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Editar Imagens</a>
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.quem-somos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.quem-somos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
