@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Equipamentos para Locação /</small> Texto</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.equipamentos-para-locacao.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.equipamentos-para-locacao.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
