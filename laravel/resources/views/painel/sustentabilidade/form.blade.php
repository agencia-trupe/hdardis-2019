@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto_home', 'Texto Home') !!}
    {!! Form::textarea('texto_home', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_1', 'Imagem 1') !!}
            @if($registro->imagem_1)
            <img src="{{ url('assets/img/sustentabilidade/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_1', 'Texto 1') !!}
            {!! Form::textarea('texto_1', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitulo']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_2', 'Texto 2') !!}
            {!! Form::textarea('texto_2', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitulo']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_2', 'Imagem 2') !!}
            @if($registro->imagem_2)
            <img src="{{ url('assets/img/sustentabilidade/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_3', 'Imagem 3') !!}
            @if($registro->imagem_3)
            <img src="{{ url('assets/img/sustentabilidade/'.$registro->imagem_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_3', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_3', 'Texto 3') !!}
            {!! Form::textarea('texto_3', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitulo']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
