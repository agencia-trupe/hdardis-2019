@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('tipo', 'Tipo') !!}
    {!! Form::text('tipo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/equipamentos/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBullets']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.equipamentos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
