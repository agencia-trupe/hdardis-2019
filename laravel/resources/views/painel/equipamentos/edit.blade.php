@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Equipamentos para Locação / Equipamentos /</small> Editar Equipamento</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.equipamentos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.equipamentos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
