import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('.banners').cycle({
    slides: '>.banner',
    'pagerTemplate': '<a href="#">{{slideNum}}</a>'
});

$('.fancybox').fancybox({
    helpers: {
        title: { type: 'inside' }
    }
});
