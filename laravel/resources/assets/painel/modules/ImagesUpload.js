export default function ImagesUpload() {
    var $wrapper = $('#images-upload');
    var errors;

    $wrapper.fileupload({
        dataType: 'json',
        start: function(e) {
            if ($('.no-images').length) $('.no-images').fadeOut();

            if ($('.errors').length) {
                errors = [];
                $('.errors').fadeOut().html('');
            }
        },
        done: function (e, data) {
            $('#imagens').prepend($(data.result.body).hide().addClass('new'))
                         .sortable('refresh');
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);

            $('.progress-bar').css(
                'width',
                progress + '%'
            );
        },
        stop: function() {
            $('.progress-bar').css('width', 0);

            $('#imagens').find('.imagem').sort(function(a, b) {
                var imgA = $(a).data('imagem').toUpperCase();
                var imgB = $(b).data('imagem').toUpperCase();
                var ordA = $(a).data('ordem');
                var ordB = $(b).data('ordem');

                if (ordA < ordB) return -1;
                if (ordA > ordB) return 1;
                if (imgA < imgB) return -1;
                if (imgA > imgB) return 1;
                return 0;
            }).appendTo($('#imagens'));

            $('#imagens .imagem.new').each(function(i) {
                $(this).delay((i++) * 400).fadeIn(300);
            });

            $('.imagem').removeClass('new');

            if (errors.length) {
                errors.forEach(function(message) {
                    $('.errors').append(message + '<br>');
                });
                $('.errors').fadeIn();
            }
        },
        fail: function(e, data) {
            var status       = data.jqXHR.status,
                errorMessage = (status == '422' ? 'O arquivo deve ser uma imagem.' : 'Erro interno do servidor.'),
                response     = 'Ocorreu um erro ao enviar o arquivo ' + data.files[0].name + ': ' + errorMessage;

            errors.push(response);
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

    // legenda

    toastr.options = {
        'positionClass': 'toast-bottom-right',
        'progressBar': true
    };

    var enviaAlteracao = function() {
        $.ajax({
            type: "POST",
            url: $('#form-modal-foto').attr('action'),
            data: { legenda: $('#form-modal-foto #legenda').val(), _method: 'PATCH' },
            success: function(data) {
                toastr.success(data.success);
                bootbox.hideAll();
            },
            error: function(data) {
                toastr.error(data.responseText);
            },
            dataType: 'json'
        });
    };

    $('body').on('submit', '#form-modal-foto', function(e) {
        e.preventDefault();
        enviaAlteracao();
    });

    $('body').on('click', '.imagem-edit-legenda', function(e) {
        e.preventDefault();

        var imagemRota = $(this).first().data('href'),
            imagemArquivo = $(this).first().data('path');

        $.getJSON(imagemRota, function(data) {
            console.log(data.legenda);
            bootbox.dialog({
                title: 'Editar Imagem',
                onEscape: function() {},
                message: '<div class="row">' +
                    '<div class="col-md-2">' +
                    '<img src="' + imagemArquivo + '/' + data.imagem +'" style="width:100%;height:auto;">' +
                    '</div>' +
                    '<div class="form-group col-md-10">' +
                    '<form id="form-modal-foto" action="' + imagemRota + '">' +
                    '<label for="legenda">Legenda</label>' +
                    '<input class="form-control" name="legenda" type="text" value="' + (data.legenda || '') + '" id="legenda">' +
                    '<input type="submit" style="visibility: hidden; position: absolute; height: 0px; width: 0px; border: none; padding: 0px;" hidefocus="true" tabindex="-1">' +
                    '</form></div></div>',
                buttons: {
                    cancel: {
                        label: 'Cancelar',
                        className: 'btn-default btn-sm'
                    },
                    main: {
                        label: '<span class="glyphicon glyphicon-edit" style="margin-right:10px;"></span>Alterar',
                        className: 'btn-success btn-sm',
                        callback: function() {
                            enviaAlteracao();
                        }
                    },
                }
            }).bind('shown.bs.modal', function(){
                $(this).find("input").first().focus();
            });
        });
    });
};
