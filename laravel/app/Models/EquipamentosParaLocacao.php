<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class EquipamentosParaLocacao extends Model
{
    protected $table = 'equipamentos_para_locacao';

    protected $guarded = ['id'];

}
