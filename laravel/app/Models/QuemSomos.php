<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class QuemSomos extends Model
{
    protected $table = 'quem_somos';

    protected $guarded = ['id'];

    public function imagens()
    {
        return $this->hasMany('App\Models\QuemSomosImagem', 'quemsomos_id')->ordenados();
    }
}
