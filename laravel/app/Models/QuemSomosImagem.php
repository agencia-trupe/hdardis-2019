<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class QuemSomosImagem extends Model
{
    protected $table = 'quem_somos_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeQuemSomos($query, $id)
    {
        return $query->where('quemsomos_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 180,
                'height'  => 180,
                'path'    => 'assets/img/quem-somos/imagens/thumbs/'
            ],
            [
                'width'   => 300,
                'height'  => 270,
                'path'    => 'assets/img/quem-somos/imagens/'
            ]
        ]);
    }
}
