<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\CropImage;

class Foto extends Model
{
    protected $table = 'galeria';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 385,
                'height'  => 260,
                'path'    => 'assets/img/galeria/thumbs/'
            ],
            [
                'width'   => null,
                'height'  => null,
                'path'    => 'assets/img/galeria/'
            ]
        ]);
    }
}
