<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Cliente extends Model
{
    protected $table = 'clientes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            'width'   => 180,
            'height'  => 110,
            'color'   => '#fff',
            'upsize'  => true,
            'path'    => 'assets/img/clientes/'
        ]);
    }
}
