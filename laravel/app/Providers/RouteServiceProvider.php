<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('galeria', 'App\Models\Foto');
		$router->model('servicos', 'App\Models\Servico');
		$router->model('imagens_servicos', 'App\Models\ServicoImagem');
		$router->model('quem-somos', 'App\Models\QuemSomos');
		$router->model('imagens_quem-somos', 'App\Models\QuemSomosImagem');
		$router->model('clientes', 'App\Models\Cliente');
		$router->model('sustentabilidade', 'App\Models\Sustentabilidade');
		$router->model('equipamentos', 'App\Models\Equipamento');
		$router->model('equipamentos-para-locacao', 'App\Models\EquipamentosParaLocacao');
		$router->model('banners', 'App\Models\Banner');
		$router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('servicos_slug', function($key) {
            return \App\Models\Servico::whereSlug($key)->firstOrFail();
        });
        $router->bind('equipamentos_slug', function($key) {
            return \App\Models\Equipamento::whereSlug($key)->firstOrFail();
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
