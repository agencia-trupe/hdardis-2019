<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('quem-somos', 'QuemSomosController@index')->name('quem-somos');
    Route::get('servicos/{servicos_slug?}', 'ServicosController@index')->name('servicos');
    Route::get('equipamentos-para-locacao/{equipamentos_slug?}', 'EquipamentosController@index')->name('equipamentos');
    Route::get('sustentabilidade', 'SustentabilidadeController@index')->name('sustentabilidade');
    Route::get('galeria', 'GaleriaController@index')->name('galeria');
    Route::get('clientes', 'ClientesController@index')->name('clientes');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::resource('galeria', 'FotosController');
		Route::resource('servicos', 'ServicosController');
		Route::get('servicos/{servicos}/imagens/clear', [
			'as'   => 'painel.servicos.imagens.clear',
			'uses' => 'ServicosImagensController@clear'
		]);
		Route::resource('servicos.imagens', 'ServicosImagensController', ['parameters' => ['imagens' => 'imagens_servicos']]);
		Route::resource('quem-somos', 'QuemSomosController', ['only' => ['index', 'update']]);
		Route::resource('quem-somos/imagens', 'QuemSomosImagensController', ['parameters' => ['imagens' => 'imagens_quem-somos']]);
		Route::resource('clientes', 'ClientesController');
		Route::resource('sustentabilidade', 'SustentabilidadeController', ['only' => ['index', 'update']]);
		Route::resource('equipamentos', 'EquipamentosController');
		Route::resource('equipamentos-para-locacao', 'EquipamentosParaLocacaoController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');
		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
