<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class QuemSomosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'texto' => 'required',
            'missao' => 'required',
            'visao' => 'required',
            'valores' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
