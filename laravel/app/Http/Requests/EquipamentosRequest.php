<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EquipamentosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'tipo' => 'required',
            'capa' => 'required|image',
            'texto' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
