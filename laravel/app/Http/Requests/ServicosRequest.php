<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServicosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'texto' => 'required',
            'imagem_home' => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem_home'] = 'image';
        }

        return $rules;
    }
}
