<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SustentabilidadeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto_home' => 'required',
            'imagem_1' => 'image',
            'texto_1' => 'required',
            'texto_2' => 'required',
            'imagem_2' => 'image',
            'imagem_3' => 'image',
            'texto_3' => 'required',
        ];
    }
}
