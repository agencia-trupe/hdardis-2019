<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Sustentabilidade;

class SustentabilidadeController extends Controller
{
    public function index()
    {
        $sustentabilidade = Sustentabilidade::first();

        return view('frontend.sustentabilidade', compact('sustentabilidade'));
    }
}
