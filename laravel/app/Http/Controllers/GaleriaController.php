<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Foto;

class GaleriaController extends Controller
{
    public function index()
    {
        $imagens = Foto::ordenados()->get();

        return view('frontend.galeria', compact('imagens'));
    }
}
