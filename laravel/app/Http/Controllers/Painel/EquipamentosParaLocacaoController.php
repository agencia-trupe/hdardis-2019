<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EquipamentosParaLocacaoRequest;
use App\Http\Controllers\Controller;

use App\Models\EquipamentosParaLocacao;

class EquipamentosParaLocacaoController extends Controller
{
    public function index()
    {
        $registro = EquipamentosParaLocacao::first();

        return view('painel.equipamentos-para-locacao.edit', compact('registro'));
    }

    public function update(EquipamentosParaLocacaoRequest $request, EquipamentosParaLocacao $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.equipamentos-para-locacao.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
