<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\QuemSomosImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\QuemSomosImagem;

use App\Helpers\CropImage;

class QuemSomosImagensController extends Controller
{
    public function index()
    {
        $imagens = QuemSomosImagem::ordenados()->get();

        return view('painel.quem-somos.imagens.index', compact('imagens'));
    }

    public function show(QuemSomosImagem $imagem)
    {
        return $imagem;
    }

    public function store(QuemSomosImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = QuemSomosImagem::uploadImagem();

            $imagem = QuemSomosImagem::create($input);

            $view = view('painel.quem-somos.imagens.imagem', compact('imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(QuemSomosImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.quem-somos.imagens.index')
                ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }
}
