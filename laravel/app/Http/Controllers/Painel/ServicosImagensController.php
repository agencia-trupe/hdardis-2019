<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ServicosImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Servico;
use App\Models\ServicoImagem;

use App\Helpers\CropImage;

class ServicosImagensController extends Controller
{
    public function index(Servico $registro)
    {
        $imagens = ServicoImagem::servico($registro->id)->ordenados()->get();

        return view('painel.servicos.imagens.index', compact('imagens', 'registro'));
    }

    public function show(Servico $registro, ServicoImagem $imagem)
    {
        return $imagem;
    }

    public function store(Servico $registro, ServicosImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = ServicoImagem::uploadImagem();
            $input['servico_id'] = $registro->id;

            $imagem = ServicoImagem::create($input);

            $view = view('painel.servicos.imagens.imagem', compact('registro', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Servico $registro, ServicoImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.servicos.imagens.index', $registro)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Servico $registro)
    {
        try {

            $registro->imagens()->delete();
            return redirect()->route('painel.servicos.imagens.index', $registro)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
