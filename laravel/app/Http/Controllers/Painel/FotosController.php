<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\FotosRequest;
use App\Http\Controllers\Controller;

use App\Models\Foto;

class FotosController extends Controller
{
    public function index()
    {
        $imagens = Foto::ordenados()->get();

        return view('painel.galeria.index', compact('imagens'));
    }

    public function show(Foto $imagem)
    {
        return $imagem;
    }

    public function store(FotosRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = Foto::uploadImagem();

            $imagem = Foto::create($input);
            $imagem->save();

            $view = view('painel.galeria.imagem', compact('imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function update(FotosRequest $request, Foto $imagem)
    {
        try {

            $imagem->update(['legenda' => $request->get('legenda')]);

            return response()->json(['success' => 'Legenda alterada com sucesso']);

        } catch (\Exception $e) {

            return 'Erro ao alterar legenda: '.$e->getMessage();

        }
    }

    public function destroy(Foto $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.galeria.index')
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }
}
