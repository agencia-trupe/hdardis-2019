<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Equipamento;
use App\Models\EquipamentosParaLocacao;

class EquipamentosController extends Controller
{
    public function index(Equipamento $equipamento)
    {
        $equipamentos = Equipamento::ordenados()->get();

        if (! $equipamento->exists) {
            $equipamento = EquipamentosParaLocacao::first();
            $equipamento->tipo = 'Equipamentos para Locação';
        }

        return view('frontend.equipamentos', compact('equipamentos', 'equipamento'));
    }
}
