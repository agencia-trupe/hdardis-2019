<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Banner;
use App\Models\Servico;
use App\Models\Sustentabilidade;

class HomeController extends Controller
{
    public function index()
    {
        $banners          = Banner::ordenados()->get();
        $servicos         = Servico::ordenados()->get();
        $sustentabilidade = Sustentabilidade::first();

        return view('frontend.home', compact('banners', 'servicos', 'sustentabilidade'));
    }
}
